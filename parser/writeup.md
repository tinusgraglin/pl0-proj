# PL/0 LR(1) Parser Implementation

## 1. 数据类型与结构规定

根据 [grammar.md](../grammar.md) 的词法和语法规定，定义关于终结符、非终结符、符号、产生式、FIRST 集表、Item、Item 集合、Canonical Collection 结构、Action Table 结构、Goto Table 结构等结构：

```ocaml
type pl0_terminal = 
    IDENTIFIER
  | NUMBER 
  | CONST 
  | VAR 
  | PROCEDURE 
  | CALL 
  | BEGIN 
  | END 
  | IF 
  | THEN 
  | WHILE 
  | DO 
  | ODD 
  | READ 
  | WRITE 
  | CONST_ASSIGN_OP 
  | IDENTIFIER_ASSIGN_OP 
  | NUMERICAL_OP_MULDIV 
  | NUMERICAL_OP_PLUSMINUS 
  | RELATIONAL_OP 
  | DOT 
  | COMMA 
  | SEMICOLON 
  | LEFT_PARENTHESIS 
  | RIGHT_PARENTHESIS

type pl0_nonterminal = 
    PROG 
  | BLOCK 
  | OPT_CONST_DECLARE 
  | CONST_DECLARE 
  | CONST_ASSIGN 
  | OPT_VAR_DECLARE 
  | IDENTIFIER_LIST 
  | OPT_PROCEDURES 
  | OPT_STATEMENT 
  | STATEMENT 
  | STATEMENT_LIST 
  | EXPRESSION_LIST 
  | CONDITION 
  | EXPRESSION 
  | ADD_SUBTRACT_EXPRESSION 
  | TERM 
  | FACTOR 
  | EXTRA_START_SYMBOL

type symbol = 
    T of pl0_terminal 
  | NT of pl0_nonterminal 
  | Empty 
  | EOF

type symseq = symbol list

type production_srhs = {lhs: pl0_nonterminal; rhs: symseq}

type production_mrhs = {lhs: pl0_nonterminal; rhs_m: symseq list}

type first_set_table = (symbol * symbol list) list

type lr1_item = {prod: production_srhs; pos: int; lookaheads: symbol list}

type itemlist_node = {number: int; flag: bool; itemlist: lr1_item list; outedges: (symbol * int) list}

type lr1_parsing_action =
    Shift of int
  | Reduce of int
  | ParseError
  | SentenceAccept

type lr1_goto_table_entry =
    Goto of int
  | GotoError

type lr1_action_table = ((int * ((symbol * lr1_parsing_action) list)) list)

type lr1_goto_table = ((int * ((symbol * lr1_goto_table_entry) list)) list)
```

## 2. LR(1) 语法分析表运算流程实现

这一部分的使用 OCaml 语言以（纯）函数式编程方法实现。

### 2.1 FIRST SET 运算

参照龙书 Chapter 4 中给出的描述和 Engineering a Compiler Chapter 3 给出的伪代码：

<img src="pic/dragon-book-first-set-calcu.png" alt="dragon-book-first-set-calcu" style="zoom:50%;" />

<img src="pic/engineering-compiler-book-first-set-calcu.png" alt="engineering-compiler-book-first-set-calcu" style="zoom:75%;" />

实现 1、计算非非终结符的FIRST SET 的过程 2、给定一个所有符号的 FIRST SET，运算一段符号串的 FIRST SET 的过程  3、给定符号集合和产生式集合，对所有符号计算 FIRST SET，然后构建 FIRST SET 表的过程。

具体实现上，我选择了 OCaml `List` 来模拟集合，由于上述运算有集合并的操作，我实现了 `List` 上的类似的 Unique Combination 操作，并自定义这个运算符为 `@|`；而对于原算法中的 Fixed Point Iteration 结构，由于（纯）函数式编程没有 `While` ，`For` 这些操作，我先定义了输出的规模函数 `xx_scale: ... -> int`，然后将迭代过程从主过程中分出来，在迭代过程中，在进行一次迭代后，新输出的规模会被计算出来，与旧规模对比，如果新规模比旧规模大，那么迭代过程调用自身进行下一次迭代（这里的递归是尾递归），否则直接返回输出；而主过程，只要在初始化一些数据后，调用迭代过程，等待它返回，然后返回它的输出即可。

```ocaml
(** unique combine *)
let ( @| ) lst1 lst2 =
  List.fold_left (fun x y -> if not (List.mem y x) then x@[y] else x) lst1 lst2

let first_set_table_scale (table: first_set_table) = 
  List.fold_left (fun x (_, lst) -> x + List.length lst) 0 table

let first_not_nterm_syms_of_symseq symseq ref_table = 
  let f_fold (l: symbol list) (r: symbol) : symbol list = 
    match List.mem Empty l with 
    | true -> (List.filter (fun b -> not (b = Empty)) l) @| List.assoc r ref_table
    | false -> l
  in List.fold_left f_fold [Empty] symseq

let first_not_nterm_syms_of_multiple_symseq symseq_lst ref_table = 
  List.fold_left (fun l r -> l @| (first_not_nterm_syms_of_symseq r ref_table)) [] symseq_lst

let updated_nonterm_first_sets_once (old_table: first_set_table) (productions: production_mrhs list) = 
  let f_fold oldtable {lhs; rhs_m} =
    let f_map (sym, symlst) = 
      if sym = NT lhs then 
        (sym, symlst @| (first_not_nterm_syms_of_multiple_symseq rhs_m oldtable)) 
      else 
        (sym, symlst) 
    in
    List.map f_map oldtable
  in
  List.fold_left f_fold old_table productions

let rec update_nonterm_first_sets_rec (productions: production_mrhs list) (table: first_set_table) old_scale =
  let new_table = updated_nonterm_first_sets_once table productions in
  let new_scale = first_set_table_scale new_table in
  let () = Printf.printf "First set: Old Scale %d -> New Scale %d\n" old_scale new_scale in
  if new_scale > old_scale then 
    update_nonterm_first_sets_rec productions new_table new_scale
  else 
    new_table

let build_initial_first_set_table (symbols: symbol list) = 
  let mapf x = 
    match x with 
    | NT _ -> (x, []) 
    | _ -> (x, [x]) 
  in 
  List.map mapf symbols

let build_first_set_table (symbols: symbol list) (productions: production_mrhs list) =
  let init_table = build_initial_first_set_table symbols in
  let init_scale = first_set_table_scale init_table in
  update_nonterm_first_sets_rec productions init_table init_scale
```

### 2.2 Closure 运算

参照龙书 Chapter 4 和 Engineering a Compiler Chapter 3 给出的伪代码：

<img src="pic/dragon-book-closure.png" alt="dragon-book-closure" style="zoom:50%;" />

<img src="pic/engineering-compiler-closure.png" alt="engineering-compiler-closure" style="zoom:67%;" />

同样地，Closure 的计算也有 Fixed Point Iteration 结构，按照之前提到的方法使用规模函数和递归实现；另外，由于我选择了合并同产生式同分析位置的 Item，上面算法中，并上 Item 的操作要更复杂一些，需要先找同产生式同分析位置的 Item，如果能找到，则将 lookaheads 并上找到的 Item 的 Lookaheads，如果找不到，则直接并上这个Item，同之前一样，我将这个特殊的运算自定义为 `@|!|`：

```ocaml
let ( @|!| ) itemlst1 itemlst2 = List.fold_left
  (fun ilst1 item_in_lst2 ->
    match item_in_lst2 with {prod=item_prod; pos=item_pos; lookaheads=item_lookaheads} -> 
    begin
      match (List.find_opt (fun {prod;pos;_} -> prod = item_prod && pos = item_pos) ilst1) with
      | Some _ -> 
        begin
          List.map 
          (fun {prod;pos;lookaheads} -> if prod = item_prod && pos = item_pos then
            {prod=prod;pos=pos;lookaheads=item_lookaheads @| lookaheads}
           else 
            {prod;pos;lookaheads}) 
          ilst1
        end
      | None -> ilst1 @ [item_in_lst2]
    end
  )
  itemlst1
  itemlst2

let itemlist_scale items = List.fold_left (fun x {prod=_; pos=_; lookaheads} -> x + List.length lookaheads) 0 items

let update_closure_once (items: lr1_item list) (productions: production_mrhs list) (ref_first_set_table: first_set_table) : lr1_item list =
  let fold_f itemlist {prod={lhs; rhs=symseq}; pos; lookaheads} = 
    match (List.nth_opt symseq pos) with
    | Some NT nterm_x -> 
      begin
        let symseq_follow_x_within_prod = List.filteri (fun i _ -> i > pos) symseq in
        let symseq_follow_x_combined = symseq_follow_x_within_prod @ lookaheads in
        let {lhs=_; rhs_m=x_prod_rhs_m} = List.find (fun {lhs;rhs_m=_} -> lhs = nterm_x) productions in
        itemlist @|!| (List.map 
        (fun symseq -> 
          {
            prod = {lhs = nterm_x;rhs = symseq}; 
            pos = if symseq = [Empty] then 1 else 0; 
            lookaheads =
            if symseq_follow_x_within_prod = [] then 
              lookaheads
            else
              (first_not_nterm_syms_of_symseq symseq_follow_x_combined ref_first_set_table)
          }
        )
        x_prod_rhs_m)
      end
    | _ -> itemlist
  in
    List.fold_left fold_f items items

let rec update_closure_rec items productions ref_first_set_table old_scale : lr1_item list =
  let updated = update_closure_once items productions ref_first_set_table in
  let new_scale = itemlist_scale updated in
  let () = Printf.printf "Closure: Old Scale %d -> New Scale %d\n" old_scale new_scale in
  if new_scale > old_scale then
    update_closure_rec updated productions ref_first_set_table new_scale
  else
    updated

let closure productions ref_first_set_table items = 
  let init_scale = itemlist_scale items in
  update_closure_rec items productions ref_first_set_table init_scale
```

注意当 Item 中的产生式右侧是 `Empty` 时，它的初始分析位置为 `1`，即初始分析位置就是结尾，逻辑上等价于这样的 Item：

$$ A\rightarrow \cdot\ \ ,\cdots $$

### 2.3 Goto 运算

参照龙书 Chapter 4 上的 有关内容：

<img src="pic/dragon-book-goto.png" alt="dragon-book-goto" style="zoom:50%;" />

很容易写出实现：

```ocaml
let goto_without_closure items symbol = 
  List.fold_left
  (fun itemlst item -> match item with {prod={lhs=_;rhs};pos;lookaheads=_} ->
    match List.nth_opt rhs pos with
    | Some s -> if s = symbol then itemlst @ [{item with pos=pos+1}] else itemlst
    | None -> itemlst
  )
  []
  items
```

注意这里我为了调试方便，没有经过 `closure` 计算。

### 2.4 Canonical Collection 运算

参照龙书 Chapter 4 和 Engineering a Compiler Chapter 3 给出的伪代码：

<img src="pic/dragon-book-cc.png" alt="dragon-book-cc" style="zoom:50%;" />

<img src="pic/engineering-compiler-cc.png" alt="engineering-compiler-cc" style="zoom:67%;" />

CC 的计算也有 Fixed Point Iteration 结构，同样按照之前提到的方法使用规模函数和递归实现；这里同样也有集合并的操作，需要类似的 Unique Combination 实现：

```ocaml
(** add an new node corresponding to [itemlst] to a canonical collection only if such an node does not exist already.
    return a tuple whose first element is the updated (or original) collection and the second element is the number 
    of newly added node (or the already-existing node containing [itemlst]). *)
let add_distinct_to_collection (itemlst: lr1_item list) (collection: itemlist_node list) = 
  let number_itemlst_opt =
  List.map (fun {number;itemlist} -> (itemlist, number)) collection |> List.assoc_opt itemlst
  in
  match number_itemlst_opt with
  | Some no -> (collection, no)
  | None -> let new_no = List.length collection in
    ({number=new_no; flag=false; itemlist=itemlst; outedges=[]} :: collection, new_no)

let update_canonical_collection_once 
(collection: itemlist_node list)
(productions: production_mrhs list) 
(ref_first_set_table: first_set_table) : itemlist_node list =
  List.fold_left
  (fun cur_coll {number=x_no;flag=x_marked;itemlist=x_itemlist} -> 
    if x_marked then
      cur_coll
    else
      begin
      let (cur_coll_with_goto_x_added, x_new_outedges) =
        List.fold_left
        (fun (coll_new, outedges) item -> 
          match item with {prod={lhs=_;rhs=item_rhs}; pos; lookaheads=_} ->
          match (List.nth_opt item_rhs pos) with
          | Some sym -> 
            begin
              match sym with
              | T _ | NT _ -> 
                begin
                  if (List.assoc_opt sym outedges) = None then
                    let x_goto_on_sym = 
                      goto_without_closure x_itemlist sym |> closure productions ref_first_set_table 
                    in
                      let (coll, node_no) = add_distinct_to_collection x_goto_on_sym coll_new in
                      (coll, (sym, node_no)::outedges)
                  else
                    (coll_new, outedges)
                end
              | _ -> (coll_new, outedges)
            end
          | None -> (coll_new, outedges)
        )
        (cur_coll, [])
        x_itemlist
      in
        List.map 
        (fun node -> match node with {number} -> 
          if number = x_no then {node with flag = true; outedges = x_new_outedges} else node
        ) 
        cur_coll_with_goto_x_added
      end
  )
  collection
  collection

let rec update_canonical_collection_rec collection productions ref_first_set_table old_scale =
  let new_coll = update_canonical_collection_once collection productions ref_first_set_table in
  let new_scale = List.length new_coll in
  let () = Printf.printf "Canonical Coll: Old Scale %d -> New Scale %d\n" old_scale new_scale in
  if new_scale > old_scale then
    update_canonical_collection_rec new_coll productions ref_first_set_table new_scale
  else
    new_coll

let canonical_collection initial_item productions ref_first_set_table =
  let initial_item_closure = closure productions ref_first_set_table [initial_item] in
  let initial_coll = [{number=0; flag=false; outedges=[]; itemlist=initial_item_closure}] in
  let initial_scale = 1 in
  update_canonical_collection_rec initial_coll productions ref_first_set_table initial_scale
```


### 2.5 Action Table, Goto Table 运算

参照龙书 Chapter 4 中给出的描述和 Engineering a Compiler Chapter 3 给出的伪代码：

<img src="pic/dragon-book-table-cons.png" alt="dragon-book-table-cons" style="zoom:50%;" />

<img src="pic/engineering-compiler-table-cons.png" alt="engineering-compiler-table-cons" style="zoom:67%;" />

实现的代码如下：

```ocaml
(** replace the default value of (k, v) pair of an association list with a new (non-default) value.
    @raise Failure if the (k, v) pair has already been filled with non-default value).  *)
let assoc_list_replace_default assoclst default key rep =
  List.map 
  (fun (k, v) -> 
    if k = key then 
      if v = default then (k, rep) else raise (Failure "Already Filled") 
    else 
      (k, v)
  ) 
  assoclst

(** replace the default values of (k, v) pairs of an association list with a new (non-default) value.
    @raise Failure if one of the (k, v) pairs has already been filled with non-default value).  *)
let assoc_list_replace_default_multikeys assoclst default keys rep =
  List.map 
  (fun (k, v) -> 
    if List.mem k keys then 
      if v = default then (k, rep) else raise (Failure "Already Filled") 
    else 
      (k, v)
  ) 
  assoclst

let build_lr1_parsing_tables
(symbols: symbol list)
(productions_mrhs: production_mrhs list)
(ref_canonical_collection: itemlist_node list) 
: ((production_srhs list) * lr1_action_table * lr1_goto_table) =
  let (action_table_row_init, goto_table_row_init) =
    List.fold_left 
    (fun (action_row, goto_row) s -> 
      match s with 
      | NT _ -> (action_row, (s, GotoError)::goto_row)
      | T _ | EOF -> ((s, ParseError)::action_row, goto_row)
      | _ -> (action_row, goto_row)
    ) 
    ([], []) 
    symbols 
  in
  let productions_srhs = 
    List.fold_left 
    (fun prods_srhs {lhs;rhs_m} -> 
      prods_srhs @ (List.map (fun symseq -> {lhs=lhs;rhs=symseq}) rhs_m)
    ) 
    [] 
    productions_mrhs
  in
  let production_srhs_to_idx_map = 
    List.mapi (fun i prod -> (prod, i)) productions_srhs
  in
  let (action_table, goto_table) =
    List.fold_left
    (fun (acc_action_table, acc_goto_table) {number=x_no;itemlist=x_itemlst;outedges=x_outedges} -> 
      let (action_table_row_for_x, goto_table_row_for_x) =
        let action_row_reduce_accept_filled =
          List.fold_left
          (fun acc_action_table_row {prod={lhs;rhs};pos;lookaheads} ->
            match List.nth_opt rhs pos with
            | None -> 
              if lhs = EXTRA_START_SYMBOL then
                assoc_list_replace_default acc_action_table_row ParseError EOF SentenceAccept
              else
                Reduce (List.assoc {lhs=lhs;rhs=rhs} production_srhs_to_idx_map) |>
                assoc_list_replace_default_multikeys acc_action_table_row ParseError lookaheads 
            | _ -> acc_action_table_row
          )
          action_table_row_init
          x_itemlst
        in
          List.fold_left
          (fun (acc_action_table_row, acc_goto_table_row) (sym, dest) ->
            match sym with
            | NT _ -> (acc_action_table_row, (assoc_list_replace_default acc_goto_table_row GotoError sym (Goto dest)))
            | T _ -> ((assoc_list_replace_default acc_action_table_row ParseError sym (Shift dest)), acc_goto_table_row)
            | _ -> (acc_action_table_row, acc_goto_table_row)
          )
          (action_row_reduce_accept_filled, goto_table_row_init)
          x_outedges
      in
        ((x_no, action_table_row_for_x)::acc_action_table, (x_no, goto_table_row_for_x)::acc_goto_table)
    ) 
    ([], [])
    ref_canonical_collection
  in
  (productions_srhs, action_table, goto_table)
```

这里主过程返回三个内容，第一个是右侧只有单串符号串形式的语法产生式列表（之前所有过程需要的是右侧有多串符号串形式的语法产生式列表），每个产生式在列表中的索引即为它的产生式号；第二、三个是 Action Table 和 Goto Table，两个表中。

注意骨架 LR(1) 分析器在语法分析时只需要有关产生式的一部分信息，即每个产生式的左侧符号和右侧符号串的长度（右侧是 `Empty` 的空产生式，其右侧的长度为 0），所以在输出给骨架 LR(1) 分析器时，不需要输出完整的产生式列表。

## 3. 骨架 LR(1) 分析器实现

这一部分使用 C 语言实现。

参照龙书 Chapter 4 和 Engineering a Compiler Chapter 3 给出的伪代码：

<img src="pic/dragon-book-skeleton.png" alt="dragon-book-skeleton" style="zoom:50%;" />

<img src="pic/engineering-compiler-skeleton.png" alt="engineering-compiler-skeleton" style="zoom:67%;" />



可以给出实现：

```c
#define PRODUCTIONS 38

#define ACTION_COLS 26
#define GOTO_COLS 18
#define STATES 248

#define MAX_STACK_SIZE 1024

#define ACTION_TABLE_FLATTEN_LEN (ACTION_COLS * STATES)
#define GOTO_TABLE_FLATTEN_LEN (GOTO_COLS * STATES)

#define INIT_STATE_NUMBER 0

enum Symbols {
    /* Terminals */
    IDENTIFIER,
    NUMBER,
    CONST,
    VAR,
    PROCEDURE,
    CALL,
    BEGIN_,
    END,
    IF,
    THEN,
    WHILE,
    DO,
    ODD,
    READ,
    WRITE,
    CONST_ASSIGN_OP,
    IDENTIFIER_ASSIGN_OP,
    NUMERICAL_OP_MULDIV,
    NUMERICAL_OP_PLUSMINUS,
    RELATIONAL_OP,
    DOT,
    COMMA,
    SEMICOLON,
    LEFT_PARENTHESIS,
    RIGHT_PARENTHESIS,
    /* Non-terminals */
    PROG,
    BLOCK,
    OPT_CONST_DECLARE,
    CONST_DECLARE,
    CONST_ASSIGN,
    OPT_VAR_DECLARE,
    IDENTIFIER_LIST,
    OPT_PROCEDURES,
    OPT_STATEMENT,
    STATEMENT,
    STATEMENT_LIST,
    EXPRESSION_LIST,
    CONDITION,
    EXPRESSION,
    ADD_SUBTRACT_EXPRESSION,
    TERM,
    FACTOR,
    EXTRA_START_SYMBOL = -1,
    /* Special symbols */
    Empty = -2,
    EOF_ = -3,
};

extern int get_next_token();

int productions_lhs_sym_and_rhs_len[PRODUCTIONS][2] = { ... }

const char *action_table_flatten[ACTION_TABLE_FLATTEN_LEN] = { ... } 

const char *goto_table_flatten[GOTO_TABLE_FLATTEN_LEN] = { ... }

struct simple_int_stack{
  int s[MAX_STACK_SIZE];
  int stack_top_pos;
};

int linenum = 1;

void stack_init(struct simple_int_stack *stack) {
  stack->stack_top_pos = -1;
}

int stack_top_ele(struct simple_int_stack *stack) {
  if (stack->stack_top_pos <= -1 || stack->stack_top_pos >= MAX_STACK_SIZE){
    printf("Error: Stack Top Unreachable.\n");
    exit(-1);
  } else {
    return stack->s[stack->stack_top_pos];
  }
}

void stack_push(struct simple_int_stack *stack, int ele) {
  if (stack->stack_top_pos < -1 || stack->stack_top_pos >= MAX_STACK_SIZE - 1) {
    printf("Error: Stack Uninitialized or Full.\n");
    exit(-1);
  } else {
    stack->s[++(stack->stack_top_pos)] = ele;
  }
}

void stack_popn(struct simple_int_stack *stack, int n) {
  if (stack->stack_top_pos + 1 < n) {
    printf("Error: Stack Pop Exhasted.\n");
    exit(-1);
  } else {
    stack->stack_top_pos -= n;
  }
}

const char *action_table_get_entry(int state_num, int symbol){
  int i, col, row;

  row = state_num;
  for (i = 0; i < ACTION_COLS; i++)
    if (action_table_cols_symbols[i] == symbol) {
      col = i;
      break;
    }
  
  if (i == ACTION_COLS) {
    printf("Error: Entry can not be found in action table\n");
    exit(-1);
  }

  return action_table_flatten[(row * ACTION_COLS) + col];
}

const char *goto_table_get_entry(int state_num, int symbol){
  int i, col, row;

  row = state_num;
  for (i = 0; i < GOTO_COLS; i++)
    if (goto_table_cols_symbols[i] == symbol) {
      col = i;
      break;
    }

  if (i == GOTO_COLS) {
    printf("Error: Entry can not be found in goto table\n");
    exit(-1);
  }

  return goto_table_flatten[(row * GOTO_COLS) + col];
}

struct simple_int_stack main_stack_struct;
struct simple_int_stack *stack = &main_stack_struct;

void lr1_parsing() {
  /* primary state variables: */
  
  int cur_sym, cur_state;

  /* temporary variables: */

  const char *action_str;
  const char *goto_str;
  int dest_state;
  int prod_num, *prod_info, prod_lhs_sym, prod_rhs_len;

  /* start: */

  stack_init(stack);
  stack_push(stack, INIT_STATE_NUMBER);

  cur_sym = get_next_token();

  while (1) {
    cur_state = stack_top_ele(stack);

    action_str = action_table_get_entry(cur_state, cur_sym);

    switch (*action_str)
    {
    case 'S':
      stack_push(stack, atoi(action_str + 1));
      cur_sym = get_next_token();
      break;
    case 'R':
      prod_num = atoi(action_str + 1);
      prod_info = productions_lhs_sym_and_rhs_len[prod_num];
      prod_lhs_sym = prod_info[0];
      prod_rhs_len = prod_info[1];

      stack_popn(stack, prod_rhs_len);
      
      goto_str = goto_table_get_entry(stack_top_ele(stack), prod_lhs_sym);

      if (*goto_str == 'G') {
        stack_push(stack, atoi(goto_str + 1));
      } else {
        printf("Error: Required goto table entry don't exist. linenum = %d\n", linenum);
        exit(-1);
      } 
      break;
    case 'A':
      goto complete;
    default:
      printf("Error: Unknown Syntax error. linenum = %d\n", linenum);
      exit(-1);
    }
  }
  complete:
  printf("Parsing Complete.\n");
}
```

其中 `get_next_token` 是词法分析器给语法分析的接口，`productions_lhs_sym_and_rhs_len` 、`action_table_flatten` 和 `goto_table_flatten` 是之前词法分析表运算流程的输出，分别存有语法分析时需要的产生式信息、Action Table 和 Goto Table 信息。


## 4. 整合、测试

将 LR(1) 词法分析表运算流程的输出导入骨架 LR(1) 分析器后，配合词法分析程序，即可进行语法分析测试：

对于以下测试用例：

```
const a = 10;
var   b, c;

procedure p;
    if a <= 10 then
        begin
            c := b + a;
        end;
begin
    read(b);
    while b # 0 do
        begin
            call p;
            write(2 * c);
            read(b);
        end;
end.
```

分析器成功将整个文件分析完全：

![res-0](pic/res-0.png)

而对于以下语法错误测试用例，分析器成功报告错误：

```
const a := 10;  
var   b, c;
//单行注释
/*  
* 多行注释  
*/
procedure p;  
    if a <= 10 then  
        begin  
            c := b + a  
        end;  
begin  
    read(b);  
    while b # 0 do
        begin  
            call p;  
            write(2 * c);  
            read(b);  
        end;  
end.
```

![res-2](pic/res-2.png)

```
const a = 10;  
var   b, c;
//单行注释
/*  
* 多行注释  
*/
procedure p;  
    if a <= 10 then  
        begin  
            c := b + a  
        end;  
begin  
    read(b);  
    while b # 0
        begin  
            call p;  
            write(2 * c);  
            read(b);  
        end;  
end.
```

![res-1](pic/res-1.png)



## 5. 总结

这次课设让我得以全身心投入函数式编程的同时又让我使用其他语言，让我对函数式编程的特点、优势和缺点有了很多认识。首先，得益于函数式编程的纯函数要求，无需考虑一直变化的状态，而是考虑状态的变化，考虑一个状态到另一个状态的变换；所谓“变量”也不再是“容器”、“储存空间”，而是“值的名字“，于是当你使用变量时，你确切地知道它的值。这样的代价则是思维方式的曲折转变和对于效率的牺牲，你不得不去思考一个从一个整体变为产生另一个整体的函数，即使前后的整体只有一丁点的不同。函数式编程也让我对递归有了新的认识，函数世界的“重复”，很大一部分都是函数的重复应用：

$$ f(f(f(...))) $$

$$ f(g(h(f(g(h(...)))))) $$

而这，正是程序设计中的递归。


## 6. 参考文献

1.  Alfred V. Aho, Ravi Sethi, Jeffrey D. Ullman. *Compilers: Principles, Techniques, & Tools* Second Edition. 2007. Addison-Wesley.
2. Keith D. Cooper, Linda Torczon. *Engineering a Compiler*. Second Edition. 2012. Morgan Kaufmann.
3. Wikipedia. *PL/0*. https://en.wikipedia.org/wiki/PL/0.
