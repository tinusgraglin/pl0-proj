# PL/0 Grammar

## Tokens

所有 Token (Terminal) 类别的 ID、匹配的文本、名称和属性值取值如下表所示：

| ID |             Matches             |     Category Name     |                  Attribute Value                   |
| :---------: | :-----------------------------: | :-------------------: | :------------------------------------------------: |
|      0      |    `[a-zA-Z]([_a-zA-Z0-9])*`    |      IDENTIFIER       |           name string of the identifier            |
|      1      |      `0 \| ([1-9][0-9]*)`       |        NUMBER         |         actual value of the number string          |
|      2      |            `"const"`            |         CONST         |                         /                          |
|      3      |             `"var"`             |          VAR          |                         /                          |
|      4      |          `"procedure"`          |       PROCEDURE       |                         /                          |
|      5      |            `"call"`             |         CALL          |                         /                          |
|      6      |            `"begin"`            |         BEGIN         |                         /                          |
|      7      |             `"end"`             |          END          |                         /                          |
|      8      |             `"if"`              |          IF           |                         /                          |
|      9      |            `"then"`             |         THEN          |                         /                          |
|     10      |            `"while"`            |         WHILE         |                         /                          |
|     11      |             `"do"`              |          DO           |                         /                          |
|     12      |             `"odd"`             |          ODD          |                         /                          |
|     13      |            `"read"`             |         READ          |                         /                          |
|     14      |            `"write"`            |         WRITE         |                         /                          |
|     15      |              `"="`              |    CONST_ASSIGN_OP    |                         /                          |
|     16      |             `":="`              | IDENTIFIER_ASSIGN_OP  |                         /                          |
|     17      |            `* \| /`             |  NUMERICAL_OP_MULDIV  |                one of `"*"`, `"/"`                 |
|     18      |            `+ \| -`             | NUMERICAL_OP_PLUSMINUS |                one of `"+"`, `"-"`                 |
|     19      | `== \| # \| < \| <= \| > \| >=` |     RELATIONAL_OP     | one of `"=="`, `"#"`, `"<"`, `"<="`, `">"`, `">="` |
|     20      |              `"."`              |          DOT          |                         /                          |
|     21      |              `","`              |         COMMA         |                         /                          |
|     22      |              `";"`              |       SEMICOLON       |                         /                          |
|     23      |              `"("`              |   LEFT_PARENTHESIS    |                         /                          |
|     24      |              `")"`              |   RIGHT_PARENTHESIS   |                         /                          |

**除以上词法结构外，词法分析器应跳过 PL/0 程序文本中的空格、Tab和换行符。**

## Non-terminals

非终结符的 ID 和名称规定如下表所示：

|ID|Name|
|:--:|:--:|
|25|PROG|
|26|BLOCK|
|27|OPT_CONST_DECLARE|
|28|CONST_DECLARE|
|29|CONST_ASSIGN|
|30|OPT_VAR_DECLARE|
|31|IDENTIFIER_LIST|
|32|OPT_PROCEDURES|
|33|OPT_STATEMENT|
|34|STATEMENT|
|35|STATEMENT_LIST|
|36|EXPRESSION_LIST|
|37|CONDITION|
|38|EXPRESSION|
|39|ADD_SUBTRACT_EXPRESSION|
|40|TERM|
|41|FACTOR|
|-1|EXTRA_START_SYMBOL|

## Special Symbols

另有两个特殊符号（空与结束符）的 ID 和名称规定如下：

|ID|Name|
|:--:|:--:|
|-2|Empty|
|-3|Eof|

## Grammar Definition

PL0 的语法由下面的产生式规定，其中小写字词为非终结符，大写字词为终结符（即 Token），`Empty` 即为上面提到的空符号：（下面的文本中使用了缩进来表示部分层次结构）

```
 0. extra_start := prog
 1. prog := block DOT
 2. block := opt_const_declare opt_var_declare opt_procedures opt_statement
    
 3. opt_const_declare := Empty 
 4.                    | const_declare SEMICOLON
 5.   const_declare := CONST const_assign 
 6.                  | const_declare COMMA const_assign
 7.   const_assign := IDENTIFIER CONST_ASSIGN_OP NUMBER
    
 8. opt_var_declare := Empty 
 9.                  | VAR identifier_list SEMICOLON
10.   identifier_list := IDENTIFIER 
11.                    | identifier_list COMMA IDENTIFIER
    
12. opt_procedures := Empty 
13.                 | opt_procedures PROCEDURE IDENTIFIER SEMICOLON block SEMICOLON
    
14. opt_statement := Empty 
15.                | statement
16.   statement := IDENTIFIER IDENTIFIER_ASSIGN_OP expression
17.              | CALL IDENTIFIER
18.              | BEGIN statement_list END
19.              | IF condition THEN statement
20.              | WHILE condition DO statement
21.              | READ LEFT_PARENTHESIS identifier_list RIGHT_PARENTHESIS
22.              | WRITE LEFT_PARENTHESIS expression_list RIGHT_PARENTHESIS
    
23.     statement_list := opt_statement 
24.                     | statament_list SEMICOLON opt_statement
25.     expression_list := expression 
26.                      | expression_list COMMA expression
    
27.     condition := ODD expression 
28.                | expression RELATIONAL_OP expression
         
29.     expression := add_subtract_expression
30.                 | NUMERICAL_OP_PLUSMINUS add_subtract_expression
31.     add_subtract_expression := term 
32.                              | add_subtract_expression NUMERICAL_OP_PLUSMINUS term
    
33.     term := factor 
34.           | term NUMERICAL_OP_MULDIV factor
    
35.     factor := IDENTIFIER 
36.             | NUMBER 
37.             | LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
```
