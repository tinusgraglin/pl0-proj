# PL/0 Compiler Project

这是实现一个 PL/0 语言编译器（的一部分）的一次尝试。 

## Current State

1. LR(1) 语法分析（在[这里](parser/)）已完成，包括构建分析表的过程和骨架 LR(1) 分析器的实现，前者使用 OCaml，后者使用 C 实现。

   由于还没有人做好词法分析器:pensive: ，这部分还包括一个利用 Flex 实现的词法分析过程 。

   使用时，在 `parser` 目录下 `make` 产生 `pl0parser` 程序之后，执行 `./pl0parser [文件] ` 即可。

   :information_source:由于现在我们的 Grammar 和 Educoder 上测试的并不一致（比如 Educoder 上，末尾分号似乎包括在 statement 的定义中，而我们的只是在 statement 列表中用分号隔开每一项），分析和错误处理方法也有的不同（我们现在的分析过程，在遇到错误之后，立即退出，而 Educoder 上的，似乎先打印出错误，跳过这一个错误，然后没事人一样接着分析），现在这个版本并不能全部通过 Educoder 上的测试。

## Grammar Description

有关词法和文法结构的定义：

- :arrow_double_up: [Grammar (22-6-11 更新)](./grammar.md)

## Interface

接口定义：

- [Lexer to Parser](./lexer-to-parser-interface.md)

## Repository Structure

```
root
   |- README.md 					这个文件
   |- grammar.md 					语法说明
   |- lexer-to-parser-interface.md 	词法分析到语法分析的接口说明
   |- lexer 						存放词法分析器的实现
          |- writeup				设计和实现说明
   |- parser						存放语法分析器的实现
          |- writeup				设计和实现说明
          |- ...                    实现相关文件
```



