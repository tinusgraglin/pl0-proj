## Lexer to Parser Interface

Parser 期待 Lexer 提供：

1. 在某个 PL/0 程序文件上初始化词法分析器的接口。

2. 在某个 PL/0 程序文件上初始化词法分析器之后，这个程序文件中所有 Token（代表终结符）的迭代器接口。

   每一个 Token 结构至少存有两类信息，第一是 Token 的类别 ID，第二是 Token 的属性值（attribute value）， 初步定下的 PL/0 Token 类别、每个类别识别的（正则表达式）文本，类别名称和类别属性值的取值见[语法说明](./grammar.md#tokens)。

接口的提供方式可以是可链接的源代码，静态、动态链接库，编译后的命令行程序甚至是 HTTP API。以 C 语言为例，接口定义可以像这样：

```c
enum TokenCategory{
    IDENTIFIER,				// 0
    NUMBER,					// 1
    CONST,					// 2
    VAR,					// 3
    PROCEDURE,				// 4
    CALL,					// 5
    BEGIN,					// 6
    END,					// 7
    IF,						// 8
    THEN,					// 9
    WHILE,					// 10
    DO,						// 11
    ODD,					// 12
    READ,					// 13
    WRITE,					// 14
    CONST_ASSIGN_OP,		// 15
    IDENTIFIER_ASSIGN_OP,	// 16
    NUMERICAL_OP_MULDIV,	// 17
    NUMERICAL_OP_PLUSMINUS	// 18
    RELATIONAL_OP,			// 19
    DOT,					// 20
    COMMA,					// 21
    SEMICOLON,				// 22
    LEFT_PARENTHESIS,		// 23
    RIGHT_PARENTHESIS		// 24
}

// lexical analyzor structure defined in your source code
struct LexicalAnalyzor;

// token structure
struct Token {
    // should have at least these two fields:
    enum TokenCategory category;
    void *p_attribute_value;
}

// initialize a lexical analyzor on a file indicated by [fp], return a pointer to it on success, or NULL on error
struct LexicalAnalyzor *initLexicalAnalyzorOn(FILE *fp);

// fills the token structure pointed by [p_token] and return 0 on success, return error code (< 0) on error
int getNextToken(struct LexicalAnalyzor *analyzor, struct Token *p_token);
```

